import React from "react";

export default () => (
  <footer>
    <p>
      <a href="/">chaseadams.io</a> is powered by GatsbyJS, GitLab & Netlify.<br />
    </p>
    <p>
      Read more about the topology in the <a href="/site-guide">Site Guide</a>.
    </p>
    <p>
      Find the source for
      <a href="https://gitlab.com/chaseadamsio/chaseadamsio.gitlab.io">
        {" "}
        chaseadams.io
      </a>{" "}
      on <a href="https://gitlab.com">Gitlab</a>
    </p>

    <div style={{ textAlign: "center", color: "#f4f4f4" }}>
      <a href="https://twitter.com/chaseadamsio">On Twitter</a> |{" "}
      <a href="https://medium.com/chaseadamsio">On Medium</a> |{" "}
      <a href="https://www.linkedin.com/in/chaseadams/">On LinkedIn</a>
    </div>
  </footer>
);
