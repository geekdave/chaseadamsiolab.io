---
title: About Me
slug: "/me/"
date: 2013-06-21
aliases: [ "/start-here/", "/now/", "/about-me", "/about-me/", "/about", "/about/"]
---

I'm Chase Adams, a Software Engineering Manager in Las Vegas, NV.

This website is a collection of thoughts about engineering, management and general effectiveness. My goal is to share what I've learned and experienced so that others can learn too.

# What I'm Doing Now

- Loving on my family
- Managing an Engineering Support Team at Walmart Labs
- Writing articles about Web Development, Management & Distributed Teams
- Learning Colemak
