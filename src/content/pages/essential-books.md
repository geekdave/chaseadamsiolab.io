---
title: "Essential Books That Changed How I Live & Work"
date: 2016-06-21T02:47:00Z
slug: essential-books
description: The canonical writings that I revisit multiple times a year because they have (and continue to) impacted my life deeply.
---

![Essential books that changed how I live and work](/img/essential-books.jpg)

Every so often you come across a piece of writing that was such a critical component to changing the way you live your life or pursue work, that it's worth revisiting. 

Not only are they worth revisiting, **they're worth evangelizing so that they help other people.** 

For myself, I have 3 such books that I re-read multiple times a year and own in multiple formats for consumption:

- [The Seven Habits of Highly Effective People]({{< ref "#the-seven-habits-of-highly-effective-people" >}})
- [Managing Oneself]({{< ref "#managing-oneself" >}})
- [Essentialism: The Disciplined Pursuit of Less]({{< ref "#essentialism-the-disciplined-pursuit-of-less" >}})

## The Seven Habits of Highly Effective People 

<img class="align-left" height="175" src="https://images-na.ssl-images-amazon.com/images/I/51hV5vGr4AL.jpg" />

**_by Stephen Covey_**

This book has been with me since I was a teen and I've read it at least once every year (often before a new year begins). 

Stephen Covey was the master of being _effective_. The title of the book clears any ambiguity: it holds the seven habits that drive highly effective people.

Choosing to intentionally practice these principles has helped me to grow tremendously and every time I read it, I walk away from some new insight because of new experiences that I didn't have the year prior.

Link: [The 7 Habits of Highly Effective People on Amazon](https://amzn.com/1451639619)

## Managing Oneself 

<img class="align-left" height="175" src="https://images-na.ssl-images-amazon.com/images/I/618d4bK5q5L._SX344_BO1,204,203,200_.jpg" />

**_by Peter Drucker_**

Managing Oneself was one of the first books I read after taking a remote position at Walmart Labs. 

I believe the key to being successful at being an impactful remote contributor is having the ability to manage oneself, and who better to guide me in that growth than the master himself?

The principles he lays out in this tiny, essay-esque book were critical for my success as a remote engineer and has continued to be a critical guide for my professional growth. 

It was so critical to my success as a remote engineer, I started a [small systematic framework based on Managing Oneself](/systematic/intro).

The conversational flow of Drucker's writing and the short nature of the book itself make it easy to consume every other month in a single sitting.

Link: [Managing Oneself by Peter Drucker on Amazon](https://amzn.com/142212312X)

## Essentialism: The Disciplined Pursuit of Less

<img class="align-left" height="175" src="https://images-na.ssl-images-amazon.com/images/I/516TXpkm6%2BL.jpg" />
**_by Gregory McKeown_**

Essentialism came to me at a time when I craved direction in my career and was drowning in a million choices.  

Essentialism is about _living life by *design* not by default._ It's about distinguishing the vital few from the trivial many. It is a systematic approach to determining *what is my highest point of contribution* and *how do I execute on it?*

Essentialism helped me to decide to hone in on what was important and eliminate everything else. Every time I read it, it helps me realign and recalibrate, making me more focused on my essential intent.

Link: [Essentialism: The Disciplined Pursuit of Less (Amazon)](https://amzn.com/0804137382)

**These books have shaped the man I am today, both in my personal and professional life** and without them, I believe I'd be less intentional and less effective. They have become the canonical guides that help me navigate the waters of life and for that I am eternally grateful.
