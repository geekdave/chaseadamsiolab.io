---
title: "What Are My Values?"
slug: values
date: 2016-11-11T17:29:07Z
---

- God foremost, Family first
- Glow in the Dark
- Spend career capital on others
- The process is the User Experience
- Be clear, not clever
- Enough is best
- Hope is not a strategy (be prepared)
- Intentional Industry
- Context, context, context
- do. document. distribute. automate.
- It’s a good day for a good day
- Til, tis: Today I learned, Today I shared

## God foremost, Family first

Every decision I make should be filtered through the two lenses first:

- Does it align with who I believe God wants me to be and who I believe God is?
- Does it move my family forward? Does it make us more connected with each other, with other people and with God?

If the answer to either of those isn't "yes", the answer to a decision is “no”.

## Glow in the Dark

“What do I need to do to make the best version of myself glow in the dark?” Glowing in the dark means choosing every day to pursue being extraordinary. Pursue a life so filled with the Grace that God’s shown me that it makes me magnetic. Choose to be better than the person you were yesterday

## Spend career capital on others

As I build career capital and influence within the organizations I belong to, I want to use that capital and influence by spending it on those who have been denied it unjustly so that they can excel. The only way the work I do and the actions I take matter is if it’s done knowing to help raise others up.

## The process is the User Experience

Processes are critical for growth, but pursuing a pure process at the expense of how it impacts people misses the purpose of the process. The value of people will always weigh more than the success of a process. Choosing to value people over the process doesn’t dilute the process, it increases its value.

## Be clear, not clever

“Clever code” and “clever language” often creates confusion. Choosing to be clear emphasizes that the value is in the message, not in the implementation.

## Enough is best

More isn’t better. Less isn’t better. Enough is best. There’s power in saying exactly what needs to be said or doing exactly what needs to be done. It doesn’t mean “settle” for what’s in front of you. It does mean to know exactly what needs to be done and do it the best you can. If you could have added more, make that the next objective and stay on task. It means only removing that which can be removed without sacrificing exactly what’s meant to be said.

## Hope is not a strategy. Be prepared.

Every day comes with new circumstances and new challenges. Hoping for every day to go well is not a strategy. The strategy is to always be aware of the challenges you’ve faced in the past and the challenges you’re likely to experience in the future and prepare for how to handle them.

## Intentional Industry

Always intentionally forwards. Always in the direction you planned. Forwards without intention is busywork. Stagnation with a plan is mediocrity. Forwards with a plan is a recipe for being effective.

## Context, context, context

Context gives our experiences and ideas purpose. Always provide context when presenting ideas or feelings or experiences, without it, they're just data points.

## do. document. distribute. automate.

Everything that can be done only once is the work we should strive for. It’s deep work.

If it can be done, but must be done more than once, always document the process. Understanding something well enough to document it creates a stronger context.

If it’s a process that can be documented but can’t yet be automated, distribute: teach others how to do it through your documentation. This allows you to alleviate yourself of toil so that you can eventually automate it and eliminate the toil altogether.

Finally, automate what you can so you can focus on deep work and through automation, allow those who toiled as well to focus on deep work as well.

## It’s a good day for a good day

It’s easy to allow negative outcomes or undesired circumstances to passively guide you into feeling that it was a bad day. Choosing that today is a good day for a good day and focusing on the wins of the day and the positive outcomes allows you to own the day, to be an active accelerant in your own success.

## Daily a Learner, Daily a Teacher

Every day learn something new. Every day share something new with someone else. Knowledge and understanding spread like wildfire when there is a constant desire to create a cycle of learning and sharing.
