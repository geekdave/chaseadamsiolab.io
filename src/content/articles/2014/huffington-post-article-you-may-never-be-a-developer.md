---
date: 2014-02-25T23:15:28Z
title: "Huffington Post Article: You May Never Become A Developer"
slug: 2014/02/huffington-post-article-you-may-never-become-a-developer
aliases: [
    posts/huffington-post-article-you-may-never-become-a-developer/
]
---

I had the honor of writing my first article for the Huffington Post on finding your path and pursuing it with every fiber of your being:

> "You may never become a programmer, but I hope you choose to give it a shot. If you decide at some point it's not for you, choose to try something else. Repeat the cycle until you find your passion. Choose to pursue it. Make time for it."

Read the whole article ["You May Never Become A Programmer" on Huffington Post](http://www.huffingtonpost.com/chase-adams/you-may-never-become-a-pr_b_4831106.html?utm_hp_ref=technology&amp;ir=Technology).
